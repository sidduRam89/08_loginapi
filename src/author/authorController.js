/* eslint-disable no-console */
const request = require('request-promise');
require('dotenv').config();

async function getAuthorsData() {
  let authorsData;
  try {
    const options = {
      url: `${process.env.LIBRARY_API}/author`,
      method: 'GET',
    };
    authorsData = await request(options);
    authorsData = JSON.parse(authorsData);
  } catch (err) {
    console.error(err);
  }
  return authorsData;
}

async function getAuthorDataById(id) {
  let authorData;
  try {
    const options = {
      url: `${process.env.LIBRARY_API}/author/${id}`,
      method: 'GET',
    };
    authorData = await request(options);
    authorData = JSON.parse(authorData);
  } catch (err) {
    console.error(err);
  }
  return authorData;
}

module.exports = {
  getAuthorsData,
  getAuthorDataById,
};
