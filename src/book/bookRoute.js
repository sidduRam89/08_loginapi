/* eslint-disable no-console */
const bookRouter = require('express').Router();
const { getBooksData, getBookByIsbn } = require('./bookContoller.js');
const { redirectLogin } = require('../../middlewares/redirectMiddleware.js');
// const { getReviewByIsbn } = require('./goodReadsReview.js');

bookRouter.get('/book', redirectLogin, async (req, res) => {
  try {
    const booksData = await getBooksData();
    res.render('book', { booksData });
  } catch (err) {
    console.error(err);
  }
});
bookRouter.get('/book/:ISBN', redirectLogin, async (req, res, next) => {
  try {
    const book = await getBookByIsbn(req.params.ISBN);
    // const review = '<h1>review</h1>';
    if (book) {
      console.log(book);
      res.render('bookDetail', { book });
    } else {
      next();
    }
  } catch (err) {
    console.error(err);
  }
});
module.exports = {
  bookRouter,
};
