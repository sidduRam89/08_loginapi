/* eslint-disable no-console */
const request = require('request-promise');

async function getBooksData() {
  let booksData;
  try {
    const options = {
      url: `${process.env.LIBRARY_API}/book`,
      method: 'GET',
    };
    booksData = await request(options);
    booksData = JSON.parse(booksData);
  } catch (err) {
    console.error(err);
  }
  return booksData;
}

async function getBookByIsbn(ISBN) {
  let bookData;
  try {
    const options = {
      url: `${process.env.LIBRARY_API}/book/${ISBN}`,
      method: 'GET',
    };
    bookData = await request(options);
    bookData = JSON.parse(bookData);
  } catch (err) {
    console.error(err);
  }
  return bookData;
}

module.exports = {
  getBooksData,
  getBookByIsbn,
};
