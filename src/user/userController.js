/* eslint-disable no-console */
const { comparePassword, generateHashPassword } = require('../../app/bcrypt.js');
const { UserModel } = require('./userModel.js');

async function loginController(name, password) {
  let isPresent;
  try {
    const userData = await UserModel.find({ name });
    if (userData.length === 0) {
      isPresent = false;
    } else {
      const hash = userData[0].password;
      if (comparePassword(password, hash)) {
        isPresent = true;
        console.log(isPresent);
      } else {
        isPresent = false;
      }
    }
  } catch (err) {
    console.error(err);
  }
  return isPresent;
}

async function signUpController(name, password) {
  let isUserPresent;
  try {
    const Person = new UserModel({
      name,
      password: generateHashPassword(password),
    });
    await Person.save();
    isUserPresent = false;
  } catch (err) {
    isUserPresent = true;
  }
  return isUserPresent;
}

async function deleteManyRecords() {
  const result = await UserModel.deleteMany();
  return result;
}

module.exports = {
  loginController,
  signUpController,
  deleteManyRecords,
};
