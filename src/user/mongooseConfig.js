const mongoose = require('mongoose');
require('dotenv').config();

const db = process.env.testDB || 'userRegistered';

console.log(process.env.MONGO_USER);
console.log(process.env.MONGO_PASSWORD);

function createMongoConnection() {
  mongoose.connect(`mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@ds159634.mlab.com:59634/loginregistration`, { useNewUrlParser: true });
}

module.exports = {
  createMongoConnection,
};
