/* eslint-disable no-console */
const userRouter = require('express').Router();
const bodyParser = require('body-parser');
const validator = require('express-validator');
const session = require('express-session');
const { redirectLogin, redirectHome } = require('./redirectMiddleware.js');
const { loginController, signUpController } = require('./userController.js');

userRouter.use(session({
  secret: 'this is a string message',
  saveUninitialized: false,
  resave: false,
}));
userRouter.use(validator());
userRouter.use(bodyParser.urlencoded({ extended: false }));

userRouter.get('/', redirectHome, (req, res) => {
  res.render('login');
});
userRouter.post('/', async (req, res) => {
  try {
    const { password, name } = req.body;
    const isPresent = await loginController(name, password);
    if (isPresent) {
      req.session.user = name;
      res.redirect('/index');
    } else {
      res.render('invalidUser');
    }
  } catch (err) {
    console.error(err);
  }
});
userRouter.get('/signup', redirectHome, (req, res) => {
  res.render('signup');
});
userRouter.post('/signup', async (req, res) => {
  try {
    const { password, name } = req.body;
    let isUserPresent;
    req.checkBody('name').notEmpty().isLength({ min: 5 }).withMessage('user name should be a length of minimum of 5');
    req.checkBody('password').isLength({ min: 8 }).withMessage('password length should be a minimum of  8 ');
    req.checkBody('confirm_password').isLength({ min: 8 }).withMessage('password length should be a minimum of  8 ').matches(password)
      .withMessage('password  and confirm password both should match');
    const errors = req.validationErrors();
    if (errors) {
      res.render('invalidInput', { errors });
    } else {
      isUserPresent = await signUpController(name, password);
      if (isUserPresent) {
        res.render('alreadyRegistered', { name });
      } else {
        res.redirect('/');
      }
    }
  } catch (err) {
    console.error(err);
  }
});
userRouter.get('/logout', redirectLogin, (req, res) => {
  res.render('logout', { name: req.session.user });
  req.session.destroy((err) => {
    console.error(err);
  });
  res.redirect('/');
});

userRouter.get('/index', redirectLogin, (req, res) => {
  res.render('index');
});

module.exports = {
  userRouter,
};
