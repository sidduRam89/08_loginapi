/* eslint-disable no-use-before-define */
const mongoose = require('mongoose');
const { createMongoConnection } = require('./mongooseConfig.js');

createMongoConnection();

mongoose.set('useCreateIndex', true);
const userSchema = new mongoose.Schema({
  name: { type: String, unique: true },
  password: String,
});

const UserModel = mongoose.model('userModel', userSchema);

module.exports = {
  UserModel,
};
