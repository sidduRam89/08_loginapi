/* eslint-disable import/no-unresolved */
const express = require('express');
const { logger, loggerPort, errorLogger } = require('../middlewares/logger.js');
const { fileNotFound } = require('../middlewares/fileNotFound.js');
const { nocache } = require('../middlewares/nocache');
const { userRouter } = require('../src/user/userRouter');
const { bookRouter } = require('../src/book/bookRoute');
const { authorRouter } = require('../src/author/authorRoute');

const port = 5500;
const app = express();
app.set('view engine', 'ejs');
app.use('/public', express.static('public'));
app.use(nocache);
// to handle logs
app.use(logger);
app.use(userRouter);
app.use(authorRouter);
app.use(bookRouter);
// to handle errors
// app.use(fileNotFound);
app.use(errorLogger);

app.listen(port, loggerPort(port));

module.exports = {
  app,
};
