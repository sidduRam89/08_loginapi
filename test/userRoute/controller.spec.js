/* eslint-disable no-undef */
const { expect } = require('chai');

const { loginController, signUpController, deleteManyRecords } = require('../../src/user/userController');

describe('signup controller', () => {
  it('signup should pass', async () => {
    const userName = 'mongo';
    const password = 'test1234';
    await deleteManyRecords();
    let result = await signUpController(userName, password);
    expect(result).to.be.equal(false);
    result = await signUpController();
    expect(result).to.be.equal(true);
  });
});

describe('login Controller', () => {
  it(' login should pass', async () => {
    const userName = 'mongo';
    const password = 'test1234';
    const result = await loginController(userName, password);
    expect(result).to.be.equal(true);
  });
});
