/* eslint-disable no-undef */
const request = require('supertest');
const { app } = require('../../app/app.js');

describe('route testing', () => {
  it('root login page', (done) => {
    request(app)
      .get('/')
      .expect(200)
      .expect(/Central Library Login Page/, done);
  });
  it('signup page', (done) => {
    request(app)
      .get('/signup')
      .expect(200)
      .expect(/Sign in Central Library Page/, done);
  });

  it('root login the page', (done) => {
    request(app)
      .post('/')
      .send('name=mongo')
      .send('password=test1234')
      .expect(302)
      .expect(/Found. Redirecting to \/index/, done);
  });
  it('signing up for the page', (done) => {
    request(app)
      .post('/signup')
      .send('name=tummy')
      .send('password=test1234')
      .send('confirm_password=test1234')
      .expect(302)
      .expect(/Found. Redirecting to \//, done);
  });
});
