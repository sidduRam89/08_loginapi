module.exports = {
  apps: [{
    name: 'login',
    script: 'app/app.js',
    watch: false,
    env: {
      PORT: 5500,
      NODE_ENV: 'production',
    },
  }],
  deploy: {
    // "production" is the environment name
    production: {
      // SSH key path, default to $HOME/.ssh
      key: '/home/siddu/Downloads/hello.pem',
      // SSH user
      user: 'ubuntu',
      // SSH host
      host: '18.224.108.47',
      // SSH options with no command-line flag, see 'man ssh'
      // can be either a single string or an array of strings
      ssh_options: 'StrictHostKeyChecking=no',
      // GIT remote/branch
      ref: 'origin/master',
      // GIT remote
      repo: 'git@gitlab.com:sidduRam89/08_loginapi.git',
      // path in the server
      path: '/home/ubuntu/login',
      // Pre-setup command or path to a script on your local machine
      'pre-deploy-local': "echo 'This is a local executed command'",
      // post-deploy action
      'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production && npm start',
    },
  },
};

