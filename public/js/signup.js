/* eslint-disable no-undef */
const password = $('#password');
const confirmPassword = $('#confirm_password');

function validatePassword() {
  if (password.val() !== confirmPassword.val() && Boolean(confirmPassword.val())) {
    confirmPassword.addClass('is-invalid');
  } else {
    confirmPassword.removeClass('is-invalid');
  }
  if (password.val().length < 8 && password.val()) {
    password.addClass('is-invalid');
  } else {
    password.removeClass('is-invalid');
  }
}
password.on('change', validatePassword);
confirmPassword.on('keyup', validatePassword);
