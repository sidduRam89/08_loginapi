function fileNotFound(req, res, next) {
  res.status(404).send('404: File Not Found');
  next();
}
module.exports = {
  fileNotFound,
};
