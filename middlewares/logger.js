/* eslint-disable import/newline-after-import */
const winston = require('winston');
const loggers = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: 'log/serverLogger.log', level: 'info' }),
  ],
});
function logger(req, res, next) {
  loggers.log({
    level: 'info',
    message: `User at route: ${req.url}`,
  });
  next();
}
function loggerPort(port) {
  loggers.log({
    level: 'info',
    message: `Server listening at port: ${port}`,
  });
}
function errorLogger(error, req, res, next) {
  loggers.error(error);
  next();
}
module.exports = {
  logger,
  loggerPort,
  errorLogger,
};
